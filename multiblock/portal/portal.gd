@tool
extends Node3D

@export var dimensions = Vector2(2,3)
@export var _exit_portal: NodePath

var exit_portal: Node3D
var tracking_portallables = []


func _ready():
	$Mesh.size = Vector3(dimensions.x, dimensions.y, 1)
	$Mesh.position.y = dimensions.y / 2
	$Mesh/Inner.size = Vector3(dimensions.x - 0.1, dimensions.y - 0.1, 1.1)
	
#	$TeleporterArea/Shape.position.y = dimensions.y / 2
#	$TeleporterArea/Shape.shape.size = Vector3(dimensions.x, dimensions.y, 0.5)
	
	$PortalPlane.dimensions = dimensions
	
	if not Engine.is_editor_hint():
		exit_portal = get_node(_exit_portal)
		$PortalPlane.character_path = NodePath("/root/Root/Character")
		$PortalPlane.exit_pivot_path = exit_portal.get_path()


func _set_dimensions(value):
	dimensions = value
	$PortalPlane.dimensions = value


func _physics_process(_delta):
	var z_to_glob = to_global(Vector3(0,0,1)) - to_global(Vector3.ZERO)
	
	var to_remove = []
	
	for body in tracking_portallables:
		var direction = global_position - body.global_position
		if direction.dot(z_to_glob) < 0:
			teleport_my_dude(body)
			to_remove.append(body)
	
	for dude in to_remove:
		tracking_portallables.erase(dude)


func teleport_my_dude(dude):
	
	# Let's move dude a bit forward to prevent flickering
	var dude_transform = dude.global_transform
	var relative_to_source = global_transform.affine_inverse() * dude_transform
	var rotated_pivot = exit_portal.global_transform.rotated_local(Vector3(0,1,0), PI)
	
	# Actually move dude to exit portal
	dude.transform = rotated_pivot * relative_to_source
	
	var is_char = dude is CharacterBody3D
	if is_char:
		var global_velocity_src = dude.velocity
		var local_velocity = to_local(global_position + global_velocity_src)
		var global_velocity = rotated_pivot * local_velocity - exit_portal.global_position
		var value = max(global_velocity_src.length(), 7)
		var final_velocity = global_velocity.normalized() * value
		
		dude.velocity = final_velocity


func _on_teleporter_area_body_entered(body):
	tracking_portallables.append(body)


func _on_teleporter_area_body_exited(body):
	tracking_portallables.erase(body)
