@tool
extends Node3D

@export var dimensions = Vector2(1.0,1.0) : set = _set_dimensions
@export var character_path: NodePath: set = _set_char_path
@export var exit_pivot_path: NodePath: set = _set_pivot

var source_cam: Camera3D
var shadow_pivot: Node3D

@onready var plane = $Mesh
@onready var viewport = $SubViewport
@onready var shadow_cam: Camera3D = $SubViewport/ShadowCam


func _ready():
	var m: ShaderMaterial = plane.mesh.surface_get_material(0).duplicate()
	m.set_shader_parameter("viewport_texture", viewport.get_texture())
	plane.set_material_override(m)
	
	var proper_size = DisplayServer.screen_get_size(0)
	viewport.size = proper_size
	viewport.size_2d_override = proper_size


func _set_dimensions(value):
	dimensions = value
	$Mesh.scale = Vector3(dimensions.x * 0.99, 0.5, dimensions.y * 0.99)
	$Mesh.position.y = dimensions.y / 2


func _set_char_path(value):
	character_path = value
	
	if Engine.is_editor_hint(): return
	if character_path == null: return
	if get_parent() == null: return
	
	var node = get_node(character_path)
	if node == null: return
	
	source_cam = node.get_cam()


func _set_pivot(value):
	exit_pivot_path = value
	
	if Engine.is_editor_hint(): return
	if get_parent() == null: return
	if str(value) == "": return
	
	shadow_pivot = get_node(value)


func _process(_delta):
	if source_cam == null: return
	if shadow_pivot == null: return
	
	var relative_to_source = global_transform.affine_inverse() * source_cam.global_transform
	var rotated_pivot = shadow_pivot.global_transform.rotated_local(Vector3(0,1,0), PI)
	shadow_cam.transform = rotated_pivot * relative_to_source
	shadow_cam.transform.origin += (shadow_pivot.to_global(Vector3(0, 0, -0.5)) - shadow_pivot.global_position)
