# 2023_01_rmc demo

I may explain what is it later.


## How to run
- Grab godot 4 (I'm using beta 16 for now, you can grab it [here][gd4_b16]);
- Download and unpack this sources;
- Launch godot, import & launch project in Project manager;
- Run project as usual (F5);

[gd4_b16]: https://downloads.tuxfamily.org/godotengine/4.0/beta16/


## License

Feel free to run and modify but please do not reuse or redistribute it.
