class_name Block
extends StaticBody3D

var is_block = true

func get_hardeness():
	return 1


func get_drop_stack_instance():
	return null


func die():
	var instance = get_drop_stack_instance()
	
	if instance != null:
		get_parent().add_child(instance)
		instance.global_position = global_position
		instance.linear_velocity = Vector3(randf() - 0.5, randf(), randf() - 0.5)
		instance.linear_velocity *= 2.5
	else:
		print("Block %s returned null from get_drop_stack_instance()" % name)
	
	queue_free()
