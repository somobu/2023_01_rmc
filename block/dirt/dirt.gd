extends Block

var stack = load("res://stack/dirt/stack_dirt.tscn")

func get_hardeness():
	return 1


func get_drop_stack_instance():
	return stack.instantiate()
