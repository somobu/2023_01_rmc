@tool
extends MeshInstance3D

@export var color = Color("#5d5e5e") : set = _set_color


func _ready():
	reload()


func _set_color(value):
	color = value
	reload()


func reload():
	var material = mesh.surface_get_material(0).duplicate() as StandardMaterial3D
	material_override = material
	material_override.albedo_color = color

