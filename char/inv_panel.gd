@tool
extends Panel

const SLOTS_COUNT = 9
const UI_SLOT_WIDTH = 64 + 2

signal inventory_changed

@export var current_slot = 0 : set = _set_slot

var stacks = [ null, null, null, null, null, null, null, null, null ]


# Called when the node enters the scene tree for the first time.
func _ready():
	recalc()


func _set_slot(value):
	current_slot = int(value)
	assert(value >= 0)
	assert(value < SLOTS_COUNT)
	recalc()
	emit_signal("inventory_changed")


func recalc():
	$Focus.position.x = current_slot * UI_SLOT_WIDTH
	
	for i in range(SLOTS_COUNT):
		if stacks[i] != null and stacks[i].count < 1:
			stacks[i].queue_free()
			stacks[i] = null
			emit_signal("inventory_changed")
		
		var icon = get_node("HBoxContainer/Item_%d/Icon" % i)
		var count = get_node("HBoxContainer/Item_%d/LCount2" % i)
		
		count.text = "" if stacks[i] == null else str(stacks[i].count)
		icon.texture = null if stacks[i] == null else load(stacks[i].get_icon())
	
	var active_stack = get_active_stack()
	if active_stack == null: $ItemName.text = ""
	else: $ItemName.text = active_stack.get_name()
		


func can_suckup(stack):
	var new_stack_type = stack.get_script().resource_path
	
	for i in range(SLOTS_COUNT):
		if stacks[i] == null: return true
		if stacks[i].get_script().resource_path == new_stack_type: return true
	
	return false


func suckup(stack):
	var new_stack_type = stack.get_script().resource_path
	
	for i in range(SLOTS_COUNT):
		if stacks[i] == null: continue
		if stacks[i].get_script().resource_path == new_stack_type:
			stacks[i].count += stack.count
			stack.queue_free()
			recalc()
			return
	
	for i in range(SLOTS_COUNT):
		if stacks[i] == null:
			stack.position = Vector3.ZERO
			stack.rotation = Vector3.ZERO
			stack.freeze = true
			stacks[i] = stack
			stack.get_parent().remove_child(stack)
#			add_child(stack)
			recalc()
			emit_signal("inventory_changed")
			return


func get_active_stack():
	return stacks[current_slot]
