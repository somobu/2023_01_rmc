extends CharacterBody3D

var mouse_sens = 5 * 0.001

const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@onready var collider_mark = $"../ColliderMark"
@onready var inventory = $Control/InvPanel
@onready var target_room = $"../Tier_0"

var collided
var collision_pos = Vector3.ZERO
var collision_normal = Vector3.ZERO


func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _physics_process(delta):
	eye_rcast()
	phys_move(delta)
	stay_vertical()


func phys_move(delta):
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	move_and_slide()
 
const fac = 0.95

func stay_vertical():
	global_rotation.x = global_rotation.x * fac
	global_rotation.z = global_rotation.z * fac


func eye_rcast():
	collided = $Camera/RayCast3D.get_collider()
	
	if collided != null and "is_block" in collided and collided.is_block:
			collision_normal = $Camera/RayCast3D.get_collision_normal()
			collision_pos = collided.global_position
			collider_mark.visible = true
			collider_mark.global_position = collision_pos
	else:
		collider_mark.visible = false


func _input(event):
	if event is InputEventMouseMotion:
		rotation.y += -event.relative.x*mouse_sens
		$Camera.rotation.x = clamp(-PI/2, $Camera.rotation.x-event.relative.y*mouse_sens, PI/2)
		
	elif event is InputEventMouseButton and !event.is_pressed():
		if event.button_index == MOUSE_BUTTON_LEFT:
			if collided != null: destroy_block()
		elif event.button_index == MOUSE_BUTTON_RIGHT:
			on_rmb()
	
	if Input.is_action_just_pressed("inv_prev"):
		if inventory.current_slot > 0:
			inventory.current_slot -= 1
	
	if Input.is_action_just_pressed("inv_next"):
		if inventory.current_slot < 8:
			inventory.current_slot += 1


func reload_hand():
	for child in $Camera/Hand.get_children(): 
		$Camera/Hand.remove_child(child)
		child.queue_free()
	
	var new_child = inventory.get_active_stack()
	if new_child != null: 
		$Camera/Hand.add_child(new_child.duplicate())


func destroy_block():
	if collided == null: return
	if not collided.has_method("die"): return
	if not collided.has_method("get_hardeness"): return
	
	if collided.get_hardeness() < 0: return
	
	collided.die()


func on_rmb():
	var target_stack = inventory.get_active_stack()
	if target_stack == null: return
	target_stack.on_rmb(self)


func _on_pickup_body_entered(body):
	if not "i_am_pickable" in body: return
	if not body.i_am_pickable: return
	
	if inventory.can_suckup(body):
		inventory.suckup(body)


func get_cam():
	return $Camera


func get_looking_block():
	return collided


func toggle_guide_book():
	$Control/GuideBook.visible = not $Control/GuideBook.visible
