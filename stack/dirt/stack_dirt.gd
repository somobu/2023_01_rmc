extends Stack

var dirt_block = preload("res://block/dirt/dirt.tscn")

func get_icon():
	return "res://stack/dirt/dirt.png"


func get_stack_name():
	return "Dirt"


func is_placable():
	return true


func get_block_instance():
	return dirt_block.instantiate()
