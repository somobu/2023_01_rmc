class_name Stack
extends RigidBody3D

var i_am_pickable = true

@export var count = 1

func get_icon():	return "res://icon.svg"
func get_stack_name():	return "Item"

func is_placable():			return false
func get_block_instance():	return null


func _ready():
	collision_layer = 1 << 0 | 1 << 1 | 1 << 2
	collision_mask  = 1 << 0 | 1 << 1 | 1 << 2
	can_sleep = false


func on_rmb(player_char):
	if !is_placable(): return
	
	var looking_at = player_char.get_looking_block()
	if looking_at == null: return
	
	var block = get_block_instance()
	if block == null: return
	
	player_char.target_room.add_child(block)
	block.global_position = player_char.collision_pos + player_char.collision_normal
	
	count -= 1
	player_char.inventory.recalc()
