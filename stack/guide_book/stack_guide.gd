extends Stack


func get_icon():
	return "res://stack/guide_book/book_icon.png"


func get_stack_name():
	return "Guide book"


func on_rmb(player_char):
	player_char.toggle_guide_book()
