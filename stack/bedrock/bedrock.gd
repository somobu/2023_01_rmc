extends Stack


func get_icon():
	return "res://stack/bedrock/icon.png"


func get_name():
	return "Bedrock"


func is_placable():
	return true
